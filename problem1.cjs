//To import the fs module in file
const fs = require("fs");

//Define the function with Two variables noOfFiles and callback function
function problem1(filePath,noOffiles,cb){

    filePath = "./Random"
    // it craete the new directory with name Random and a call back function
    fs.mkdir(filePath,(error)=>{
        // if error then to send to callBack function
        if(error){
            return cb(error);
        }
        //iterating the loop to create the new files 
        for(let index=0;index<noOffiles;index++){
            let filename=`file${index}.json`;
            //for each iteration sending the value one by one
            let insertDataInFile= {
                index : index 
            }
                
            let path=`${filePath}/${filename}`;
            // each iteration it creates the write file in Random directory
            fs.writeFile(path,JSON.stringify(insertDataInFile),(error)=>{
                if(error){
                    return cb(error);
                }

            })
        }
    //  After creation of new files this loop deleting the each files
        for(let i=0;i<noOffiles;i++){
            fs.unlink(`${filePath}/file${i}.json`,(error)=>{
                if(error){
                    return cb(error);
                } 
            })
        }
        console.log("Created and Deleted Successfully")

    })
}

module.exports=problem1;
