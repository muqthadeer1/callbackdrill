//import fs module 
const fs = require("fs");


function fsProblem2(){

 //create a file path of text file
const filepath = 'lipsum.txt';
// fs module to read the data from given file
fs.readFile(filepath, 'utf8', (error, data) => {
    if (error) {
      console.log(error);
    }
   //create a variable to store the uppercase data
    let upperCaseContent = data.toUpperCase();
    // create new file name upperCase.txt   
    let upperCaseFile = "upperCase.txt";
    // it execute the file 
    fs.writeFile(upperCaseFile,upperCaseContent,"utf8",(error)=>{
        if(error){
            console.log(error);
        }
        
        //it add the file name to the other file 
    fs.appendFile("filenames.txt",upperCaseFile + "\n" ,(error)=>{
        if(error){
            console.log(error)
        }

     fs.readFile(upperCaseFile,"utf8",(error,data)=>{
        if(error){
            console.log(error);
        }

        let lowerCase = data.toLocaleLowerCase().split('.').join('\n');
        let lowerCaseFile = "lowerCase.txt";

        fs.writeFile(lowerCaseFile,lowerCase,"utf8",(error)=>{
            if(error){
                console.log(error);
            }

            fs.appendFile("filenames.txt",lowerCaseFile + "\n" ,(error)=>{
                if(error){
                    console.log(error)
                }

                fs.readFile(upperCaseFile,"utf8",(error,upperCaseData)=>{
                    if(error){
                        console.log(error);
                    }
                    

                    fs.readFile(lowerCaseFile,'utf8',(error,lowerCaseData)=>{
                        if(error){
                            console.log(error)
                        }
                        //create a variables to store the sorted data upperCaseSorted and lowerCasesorted
                        let upperCaseSorted = upperCaseData.split(' ').sort().join(' ');
                        let lowerCaseSorted = lowerCaseData.split(' ').sort().join(' ');
                        
                        //create the variable to join both the upperCase and lowerCase sentences
                        let upperAndLowerSorted = upperCaseSorted + '\n' + lowerCaseSorted
                         let upperAndLowerSortedPath = "sortedContent.txt"

                        fs.writeFile(upperAndLowerSortedPath,upperAndLowerSorted,'utf8',(error)=>{
                            if(error){
                                console.log(error);
                            }

                            fs.appendFile('filenames.txt',upperAndLowerSortedPath +'\n',(error)=>{
                                if(error){
                                console.log(error);
                                }
                                fs.readFile('filenames.txt','utf8',(error,data)=>{
                                    if(error){
                                        console.log(error)
                                    }
                                        let numberOfFiles = data.trim().split('\n');
                                        numberOfFiles.forEach((eachFile)=>{
                                        fs.unlink(eachFile,(error)=>{
                                            if(error){
                                                console.log(error);
                                            }
                                        })
                                            
                                        
                                            
                                        })
                                })
                            })
                        })
                    })
                
                })

            })
        })
     })

    })

    
    })

     

})
}

module.exports = fsProblem2;