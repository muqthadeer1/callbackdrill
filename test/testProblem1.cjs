// create a variable to import the function from other directory.
let problem1=require("../problem1.cjs");

// number of files to be create 
let noOffiles=4;
let filePath = "../lipsum.txt"

//call the function with two variables noOffiles and callback function
problem1(filePath,noOffiles,(error)=>{
    // if any error 
    if(error){
        console.log(error);
    }

});